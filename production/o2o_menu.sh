#!/bin/bash

#CMSSW_10_6_1
#/data/O2O/L1T/o2oMenu2019_v1.sh $1 $2 $3

#CMSSW_10_6_8
#/data/O2O/L1T/o2oMenu2020_v1.sh $1 $2 $3

#CMSSW_10_6_8 + fractional prescales patch
#/data/O2O/L1T/o2oMenu2020_v1_test.sh $1 $2 $3

#CMSSW_11_1_0_patch2
/data/O2O/L1T/o2oMenu2020_v2.sh $1 $2 $3

o2ocode1=$?

exit ${o2ocode1}

