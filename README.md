## L1Trigger O2O Scripts
The scripts that are included in here are called by the function manager to update the L1T related tags in the CondDB.  
Is expecter that this repository is capable to produce a package (rpm) including all the required scripts for a specific CMSSW release to run.  
The versioning of the rpms are following the scheme below:
`{major}.{minor}.{cmssw_rel}-{rel}`
eg `1.0.1115-1` is related with `CMSSW_11_1_5`

### Workflow for applying changes
Follow the next method for applying changes and push to the P5 cernbox  
**PLEASE OPEN A NEW PR FOR EACH CHANGE**
```bash
git clone https://:@gitlab.cern.ch:8443/cms-cactus/ops/l1t-o2o.git
cd l1t-o2o && git checkout -b <dev_branch>
## do changes
## if only need to update CMSSW version, e.g.
source build/setup.sh
UpdateScripts.sh 2021_v6 slc7_amd64_gcc900 CMSSW_11_3_2
# check updated scripts
git commit -am "..."
git push origin HEAD
```
**PLEASE KEEP IN MIND THAT CHANGING THE `spec` FILE IS MANDATORY OTHERWISE THE rpm WILL NOT BE UPDATED**  
With the new branch open a new PR and run the tests in the associated pipeline. An example PR can be found here https://gitlab.cern.ch/cms-cactus/ops/l1t-o2o/-/merge_requests/2.  
Once you're happy with the results, please link the PR with a JIRA ticket and use the last job in the pipeline to push the new rpm in the p5-dropbox.  
An example JIRA can be found here https://its.cern.ch/jira/browse/CMSONS-12954.

### ✨ Method to make `_test` scripts automatically ✨
The script `build/makeTestScript.sh` is capable to generate a test script from an input script for using it with PrepDB.  
The following are example commands.
```bash
makeTestScript.sh o2oMenu2021_v2.sh
makeTestScript.sh o2oSubs2021_v2.sh
```

### How to build locally
The following command can be used to build the rpm manually.
```bash
LOCAL_DIR=$PWD rpmbuild --target=noarch -bb build/o2ol1t-scripts.spec
```

### Local Test
The o2o local test runs on the gitlab runner and the output is **not** pushed to the prepdb. The CMSSW version to be used is specified in the `.gitlab-ci.yml` file. Please update the lines when you want to test with new version.
```yaml
Local Test:
    ...
     variables:
       RELEASE: CMSSW_11_2_4
       ARCH: slc7_amd64_gcc900
```
`RUNNUMBER`, `TSC_KEY`, `RS_KEY` can be configured when triggering the manual job. If not specified, it will run with default values, `380010 l1_trg_cosmics2020/v9 l1_trg_rs_cosmics2020/v21`. SSH is not needed in this job.

### PrepDB Test
The o2o prepdb test runs on `cms-conddb-1(or 2)`. It requires SSH connection to conddb host, so `P5_USER`, `P5_PASS` should be set when triggering the job. The o2o test scripts to be used is specified in `.gitlab-ci.yml` file. Please update the scripts accordingly.
```yaml
O2O Menu Test:
    ...
     variables:
       ...
       O2O_TEST_SCRIPT: o2oMenu2021_v3_test.sh

O2O Subs Test:
    ...
     variables:
       ...
       O2O_TEST_SCRIPT: o2oSubs2021_v3_test.sh
```
`RUNNUMBER`, `TSC_KEY`, `RS_KEY` can be configured when triggering the manual job. If not specified, it will run with default values, `380010 l1_trg_cosmics2020/v9 l1_trg_rs_cosmics2020/v21`.
After the job passed, you can always double check via o2o monitering web: https://cms-conddb.cern.ch/cmsDbBrowser/logs/O2O_logs/Prep/
