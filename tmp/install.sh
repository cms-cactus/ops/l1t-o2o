#!/bin/bash

. /opt/offline/cmsset_default.sh
scram project CMSSW CMSSW_8_0_1
cd CMSSW_8_0_1
eval `scram runtime -sh`
# unless the tunnel is established, run under you personal account this:
# ssh -f -N -D 1080 cmsusr.cms
git clone ssh://git@github.com/kkotov/l1o2o src
cd src
mkdir CondCore
cp -r /opt/offline/slc6_amd64_gcc493/cms/cmssw/CMSSW_8_0_1/src/CondCore/L1TPlugins/ CondCore/
# edit the plugins.cc to include the *Ext records or simply use patch7.tgz
mkdir CondFormats
cp -r /opt/offline/slc6_amd64_gcc493/cms/cmssw/CMSSW_8_0_1/src/CondFormats/{DataRecord,L1TObjects} CondFormats/
# add *Ext formats + modify the "dependent records" to include the *Ext formats or simply use patch7.tgz
tar -xzf patch7.tgz

export UTM_XSD_DIR=/data/O2O/L1T/pro/CMSSW_8_0_1/utm/tmXsd

cmsRun CMSSW_8_0_1/src/CondTools/L1TriggerExt/test/init_cfg.py useO2OTags=1 outputDBConnect=sqlite:o2o/l1config.db outputDBAuth=./o2o/

cmsRun ../CMSSW_8_0_1/src/CondTools/L1TriggerExt/test/init_cfg.py useO2OTags=1 outputDBConnect=oracle://cms_orcoff_prep/CMS_CONDITIONS outputDBAuth=.

cmsRun ../CMSSW_8_0_1/src/CondTools/L1TriggerExt/test/L1ConfigWriteSinglePayloadExt_cfg.py objectKey=Fake objectType=L1TMuonEndcapParams recordName=L1TMuonEndcapParamsRcd useO2OTags=1 outputDBConnect=oracle://cms_orcoff_prep/CMS_CONDITIONS outputDBAuth=.
cmsRun ../CMSSW_8_0_1/src/CondTools/L1TriggerExt/test/L1ConfigWriteSingleIOVExt_cfg.py objectKey=Fake objectType=L1TMuonEndcapParams recordName=L1TMuonEndcapParamsRcd condIndex=5 runNumber=1 useO2OTags=1 outputDBConnect=oracle://cms_orcoff_prep/CMS_CONDITIONS outputDBAuth=.

cmsRun ../CMSSW_8_0_1/src/CondTools/L1TriggerExt/test/L1ConfigWriteSinglePayloadExt_cfg.py objectKey=Fake objectType=L1TMuonOverlapParams recordName=L1TMuonOverlapParamsRcd useO2OTags=1 outputDBConnect=oracle://cms_orcoff_prep/CMS_CONDITIONS outputDBAuth=.
cmsRun ../CMSSW_8_0_1/src/CondTools/L1TriggerExt/test/L1ConfigWriteSingleIOVExt_cfg.py objectKey=Fake objectType=L1TMuonOverlapParams recordName=L1TMuonOverlapParamsRcd condIndex=5 runNumber=1 useO2OTags=1 outputDBConnect=oracle://cms_orcoff_prep/CMS_CONDITIONS outputDBAuth=.

cmsRun CMSSW_8_0_1/src/CondTools/L1TriggerExt/test/L1ConfigWriteSinglePayloadExt_cfg.py objectKey="OMTF_BASE_CONF/v7:OMTF_RS_BASE/v2" objectType=L1TMuonOverlapParams recordName=L1TMuonOverlapParamsO2ORcd useO2OTags=1 outputDBConnect=sqlite:o2o/l1config.db outputDBAuth=/data/O2O/L1T/dev/o2o
