#!/usr/bin/env python
'''
'''

__author__ = 'Giacomo Govi'

import CondCore.Utilities.o2o as o2olib
import sys
import optparse
import argparse
    
def main( argv ):

    parser = argparse.ArgumentParser()
    parser.add_argument("executable", type=str, help="wrapper for O2O jobs")
    parser.add_argument("-n","--name", type=str, help="the O2O job name" )
    parser.add_argument("-d","--dev", action="store_true", help="bookkeeping in dev database")
    parser.add_argument("-p","--private", action="store_true", help="bookkeeping in private database")
    parser.add_argument("-a","--auth", type=str,  help="path of the authentication file")
    args = parser.parse_args()  

    if not args.name:
        parser.error("Job name not given.")

    command = args.executable

    db_service = None
    if not args.private:
        if args.dev:
            db_service = o2olib.dev_db_service
        else:
            db_service = o2olib.prod_db_service

    class O2ORunMgrMy(o2olib.O2ORunMgr):

        def __init__( self ):
           O2ORunMgr.__init__(self)

        def executeJob( self, job_name, command ):

            logFolder = '/data/O2O/L1T/logs'

            datelabel = datetime.now().strftime("%y-%m-%d-%H-%M-%S")
            logFileName = '%s-%s.log' %(job_name,datelabel)
            logFile = os.path.join(logFolder,logFileName)
            exists, enabled = self.startJob( job_name )
            if exists is None:
                return 3
            if enabled is None:
                O2OMgr.logger( self).error( 'The job %s is unknown.', job_name )
                return 2
            else:
                if enabled == 0:
                    O2OMgr.logger( self).info( 'The job %s has been disabled.', job_name )
                    return 0
            try:
                O2OMgr.logger( self ).info('Executing job %s', job_name )
                pipe = subprocess.Popen( command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT )
                out = pipe.communicate()[0]
                O2OMgr.logger( self ).info( 'Job %s returned code: %s' %(job_name,pipe.returncode) )
            except Exception as e:
                O2OMgr.logger( self ).error( str(e) )
                return 4
            self.endJob( pipe.returncode, out )
            with open(logFile,'a') as logF:
                logF.write(out)
            return pipe.returncode


    runMgr = O2ORunMgrMy()
    ret = -1
    if runMgr.connect( db_service, args.auth ):
        ret = runMgr.executeJob( args.name, command )
    return ret

if __name__ == '__main__':
    sys.exit(main(sys.argv))
