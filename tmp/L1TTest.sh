BASEDIR=/data/O2O
RELEASE=CMSSW_8_0_10
RELEASEDIR=/data/O2O/cmssw/${RELEASE}
LOGFILE=${BASEDIR}/L1T/logs/L1TTest.log
JOBDIR=${BASEDIR}/L1T/v9_20160712
DATE=`date`

echo "----- new job started for L1T Test (run $1) -----" | tee -a $LOGFILE
echo $DATE | tee -a $LOGFILE
export O2O_LOG_FOLDER=/data/O2O/L1T/logs/
export O2O_AUTH_PATH=$BASEDIR
source /opt/offline/cmsset_default.sh

cd ${RELEASEDIR}/src
eval `scramv1  run -sh`

cd ${JOBDIR}
o2oRun.py -d -n L1TTest "/bin/sh o2o-setIOV-l1Key.sh -x $1 $2 $3" | tee -a  $LOGFILE
