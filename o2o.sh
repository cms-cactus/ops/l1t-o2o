#!/bin/bash

# CMSSW_9_2_15 + fixes of K.Kotov from 9_2_2
#/data/O2O/L1T/o2o2018_backport.sh $1 $2 $3

# CMSSW_10_0_0 + patches
#/data/O2O/L1T/o2o2018_v3.sh $1 $2 $3

# CMSSW_10_1_2
#/data/O2O/L1T/o2o2018_v4.sh $1 $2 $3

# CMSSW_10_1_2 + EMTF fix (PR 23288)
#/data/O2O/L1T/o2o2018_v5.sh $1 $2 $3

# CMSSW_10_1_6 + EMTF fix (PR 23288) + tagName update
#/data/O2O/L1T/o2o2018_v6.sh $1 $2 $3

#CMSSW_10_1_6 no calo Special run
#/data/O2O/L1T/o2o2018_v6_noCALO.sh $1 $2 $3

#CMSSW_10_1_11 (utm 0.7.x)
#/data/O2O/L1T/o2o2018_v7.sh $1 $2 $3

#CMSSW_10_1_11 no calo (utm 0.7.x)
#/data/O2O/L1T/o2o2018_v7_noCALO.sh $1 $2 $3

#CMSSW_10_3_0_pre6 no calo 
#/data/O2O/L1T/o2o2018_v8_noCALO.sh $1 $2 $3

#CMSSW_10_3_0_pre6
#/data/O2O/L1T/o2o2018_v8.sh $1 $2 $3

#CMSSW_10_3_0 + PR 25010
#/data/O2O/L1T/o2o2018_v9.sh $1 $2 $3

#CMSSW_10_3_1
#/data/O2O/L1T/o2o2018_v10.sh $1 $2 $3

#CMSSW_10_6_1_patch1
/data/O2O/L1T/o2o2019_v1.sh $1 $2 $3

#CMSSW_10_6_1_patch1
#/data/O2O/L1T/o2o2019_v1_test.sh $1 $2 $3

o2ocode1=$?

exit ${o2ocode1}

