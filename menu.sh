BASEDIR=/data/O2O/
ARCH=slc6_amd64_gcc493
RELEASE=CMSSW_8_0_18
RELEASEDIR=/opt/offline/${ARCH}/cms/cmssw/${RELEASE}
LOGFILE=${BASEDIR}/logs/L1TMenu.log
JOBDIR=${BASEDIR}/L1T/pro/
DATE=`date`

echo "----- new job started for L1T menu (run $1) -----" | tee -a $LOGFILE
echo $DATE | tee -a $LOGFILE
export O2O_LOG_FOLDER=/data/O2O/logs/L1TMenu
export O2O_AUTH_PATH=$BASEDIR/L1T/
export COND_AUTH_PATH=$BASEDIR/L1T/
source /opt/offline/cmsset_default.sh

cd ${RELEASEDIR}/src
eval `scramv1  run -sh`

cd ${JOBDIR}
o2oRun.py -n L1TMenu "/bin/sh runL1-O2O-iov.sh -x $1 $2 $3" | tee -a  $LOGFILE
o2ocode=${PIPESTATUS[0]}

exit ${o2ocode}
