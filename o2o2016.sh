BASEDIR=/data/O2O/
ARCH=slc6_amd64_gcc530
RELEASE=CMSSW_8_4_0
#RELEASEDIR=/opt/offline/${ARCH}/cms/cmssw/${RELEASE}
JOBDIR=${BASEDIR}/L1T/v10.0_shadow/
RELEASEDIR=${JOBDIR}/${RELEASE}
LOGFILE=${JOBDIR}/L1TMenu.log
DATE=`date`

echo "`date` : /bin/sh runL1-O2O-iov.sh $1 $2 $3" | tee -a $JOBDIR/o2o-summary
echo "----- new job started for L1T menu (run $1) -----" | tee -a $LOGFILE
START=$(date +%s)

echo $DATE | tee -a $LOGFILE
export O2O_LOG_FOLDER=/data/O2O/logs/L1TMenu
export O2O_AUTH_PATH=$BASEDIR/L1T/
export COND_AUTH_PATH=$BASEDIR/L1T/
source /opt/offline/cmsset_default.sh

cd ${RELEASEDIR}/src
eval `scramv1  run -sh`

cd ${JOBDIR}
#o2oRun.py -n L1TMenu "/bin/sh 
./runL1-O2O-iov.sh $1 $2 $3 | tee -a  $LOGFILE
o2ocode=${PIPESTATUS[0]}

END=$(date +%s)
DIFF=$(( $END - $START ))
if [ ${DIFF} -gt 60 ]
    then
    echo "O2O SLOW: `date`, ${DIFF} seconds for $1 $2 $3" | tee -a $JOBDIR/o2o-summary
else
    echo "Time elapsed: ${DIFF} seconds" | tee -a $JOBDIR/o2o-summary
fi
echo "" | tee -a $JOBDIR/o2o-summary


exit ${o2ocode}
