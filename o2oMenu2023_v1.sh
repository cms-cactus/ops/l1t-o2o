BASEDIR=/data/O2O
ARCH=el8_amd64_gcc11
RELEASE=CMSSW_13_0_0
RELEASEDIR=/opt/offline/${ARCH}/cms/cmssw/${RELEASE}
LOGFILE=${BASEDIR}/L1T/logs/L1TMenu.log
JOBDIR=${BASEDIR}/L1T/v25.0

echo "`date` : /bin/sh runL1-O2O-iov.sh -x TEST $1 $2 $3" | tee -a $JOBDIR/o2o-summary
echo "----- new job started for L1T menu (run $1) -----" | tee -a $LOGFILE
START=$(date +%s)

echo $DATE | tee -a $LOGFILE
export O2O_LOG_FOLDER=/data/O2O/L1T/logs/L1TMenu
export O2O_AUTH_PATH=$BASEDIR/L1T/
export COND_AUTH_PATH=$BASEDIR/L1T/
export RELEASEDIR
source /opt/offline/cmsset_default.sh

cd ${RELEASEDIR}/src

eval `scramv1  run -sh`

export TNS_ADMIN=/etc

rm $JOBDIR/lastLogForFM.txt

cd ${JOBDIR}

## run the o2o menu job
o2o --db pro run -n L1TMenu "/bin/sh runL1-O2O-iov.sh -x -k uGT,uGTrs -d uGMT,OMTF,BMTF,CALO,EMTF -t L1TGlobalPrescalesVetosFract:Stage2v1_hlt $1 $2 $3" | tee -a $LOGFILE
o2ocode=${PIPESTATUS[0]}

END=$(date +%s)
DIFF=$(( $END - $START ))
if [ ${DIFF} -gt 60 ]
    then
    echo "O2O SLOW: `date`, ${DIFF} seconds for $1 $2 $3" | tee -a $JOBDIR/o2o-summary
else
    echo "Time elapsed: ${DIFF} seconds" | tee -a $JOBDIR/o2o-summary
fi
echo "" | tee -a $JOBDIR/o2o-summary

cat $JOBDIR/lastLogForFM.txt | sed -n -e 's/SummaryForFunctionManager://gp'

exit ${o2ocode}
