BASEDIR=/data/O2O/
ARCH=slc7_amd64_gcc630
RELEASE=CMSSW_10_0_0
#RELEASEDIR=/opt/offline/${ARCH}/cms/cmssw/${RELEASE}
LOGFILE=${BASEDIR}/logs/L1TMenu.log
JOBDIR=${BASEDIR}/L1T/v12.0/
#LOGFILE=${JOBDIR}/logs/L1TMenu.log
#LOGFILE=${JOBDIR}/logs/L1TMenu_test_fromMarch6.log
RELEASEDIR=${JOBDIR}/${RELEASE}
DATE=`date`


echo "`date` : /bin/sh runL1-O2O-iov.sh -x $1 $2 $3" | tee -a $JOBDIR/o2o-summary
echo "----- new job started for L1T menu (run $1) -----" | tee -a $LOGFILE
START=$(date +%s)

echo $DATE | tee -a $LOGFILE
export O2O_LOG_FOLDER=/data/O2O/logs/L1TMenu
export O2O_AUTH_PATH=$BASEDIR/L1T/
export COND_AUTH_PATH=$BASEDIR/L1T/
source /opt/offline/cmsset_default.sh

cd ${RELEASEDIR}/src

eval `scramv1  run -sh`

export TNS_ADMIN=/data/ext/oracle-env/29/etc

rm $JOBDIR/lastLogForFM.txt

cd ${JOBDIR}
#o2oRun.py -n L1TMenu "/bin/sh runL1-O2O-iov.sh -x -t L1TMuonEndcapParams:Stage2v1_hlt $1 $2 $3" | tee -a $LOGFILE
#o2oRun.py -n L1TMenu "/bin/sh runL1-O2O-iov.sh -x -k uGT,uGTrs,uGMT,OMTF,BMTF,CALO  -t L1TMuonBarrelParams:Stage2v1_hlt $1 $2 $3" | tee -a $LOGFILE
o2oRun.py -n L1TMenu "/bin/sh runL1-O2O-iov.sh -x -k uGT,uGTrs,uGMT,OMTF,BMTF,CALO  $1 $2 $3" | tee -a $LOGFILE

# Run without -x option to write to sqlite file instead to ORCON
#o2oRun.py -n L1TMenu "/bin/sh runL1-O2O-iov.sh -k uGT,uGTrs,uGMT,OMTF,BMTF,CALO  $1 $2 $3" | tee -a $LOGFILE
#o2oRun.py -n L1TMenu "/bin/sh runL1-O2O-iov.sh -k uGT,uGTrs,GMT,OMTF,BMTF,CALO  -t L1TMuonEndCapParams:Stage2v1_hlt $1 $2 $3" | tee -a $LOGFILE
#o2oRun.py -n L1TMenu "/bin/sh runL1-O2O-iov.sh  -t L1TMuonEndCapParams:Stage2v1_hlt $1 $2 $3" | tee -a $LOGFILE
#o2oRun.py -n L1TMenu "/bin/sh runL1-O2O-iov.sh  $1 $2 $3" | tee -a $LOGFILE

#/bin/sh runL1-O2O-iov.sh  -k uGT $1 $2 $3 | tee -a $LOGFILE
#/bin/sh runL1-O2O-iov.sh  -t L1TMuonEndCapParams:Stage2v1_hlt $1 $2 $3 | tee -a $LOGFILE
#/bin/sh runL1-O2O-iov.sh   -k uGT,uGTrs,uGMT,OMTF,BMTF,CALO -t L1TMuonEndCapParams:Stage2v1_hlt $1 $2 $3 | tee -a $LOGFILE
#/bin/sh runL1-O2O-iov.sh   -k uGT,uGTrs,uGMT,OMTF,BMTF,CALO,EMTF $1 $2 $3 | tee -a $LOGFILE

#/bin/sh runL1-O2O-iov.sh  $1 $2 $3 | tee -a $LOGFILE

o2ocode=${PIPESTATUS[0]}

END=$(date +%s)
DIFF=$(( $END - $START ))
if [ ${DIFF} -gt 60 ]
    then
    echo "O2O SLOW: `date`, ${DIFF} seconds for $1 $2 $3" | tee -a $JOBDIR/o2o-summary
else
    echo "Time elapsed: ${DIFF} seconds" | tee -a $JOBDIR/o2o-summary
fi
echo "" | tee -a $JOBDIR/o2o-summary

cat $JOBDIR/lastLogForFM.txt | sed -n -e 's/SummaryForFunctionManager://gp'

exit ${o2ocode}
