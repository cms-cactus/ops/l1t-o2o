package main

import (
	"flag"
	"log"
	"p5-conddb/internal"
)

var viaLXPlus = flag.Bool("lxplus", false, "set true to pass via lxplus first")

func main() {

	O2OJob, err := internal.GetO2OTestArgs()
	if err != nil {
		log.Fatalf("cannot construct O2O test command: %s", err)
	}

	sshClient, err := internal.GotoCMSConddb(*viaLXPlus)
	if err != nil {
		log.Fatalf("cannot connect to conddb: %s", err)
	}
	
	err = internal.O2OTest(sshClient, O2OJob)
	if err != nil {
		log.Fatalf("could not execute O2O test command: %s", err)
	}
}
