package internal

import (
	"fmt"
	"os"
	"strings"

	"golang.org/x/crypto/ssh/terminal"
)

// GetCredentials fetches SSH username and password for a given hostname
func GetCredentials(host string) (string, string) {
	envPostfix := strings.ReplaceAll(strings.ToUpper(host), ".", "_")
	usernameKey := "USERNAME_" + envPostfix
	username := os.Getenv(usernameKey)
	if username == "" {
		username = os.Getenv("SSH_USERNAME")
	}
	if username == "" {
		username = os.Getenv("USERNAME")
	}
	if username == "" {
		fmt.Fprintln(os.Stderr, usernameKey+" not set")
		os.Exit(1)
	}
	passKey := "PASS_" + envPostfix
	pass := os.Getenv(passKey)
	if pass == "" {
		pass = os.Getenv("SSH_PASS")
	}
	if pass == "" {
		fmt.Print("Password for " + username + "@" + host + ": ")
		b, err := terminal.ReadPassword(0)
		if err == nil {
			pass = string(b)
		}
		fmt.Println("")
	}
	if pass == "" {
		fmt.Fprintln(os.Stderr, "password not given, nor set in "+passKey)
		os.Exit(1)
	}
	return username, pass
}
