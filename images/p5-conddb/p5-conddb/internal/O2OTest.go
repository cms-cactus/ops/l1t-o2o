package internal

import (
	"fmt"
	"bytes"
	"log"
	"os"

	"golang.org/x/crypto/ssh"
)

// Args needed for o2o test
type O2OTestArgs struct {
	TestScript   string
	RunNumber    string
	TSCKey       string
	RSKey        string
}

// GetO2OTestArgs checks if o2o args were given
func GetO2OTestArgs() (*O2OTestArgs, error) {

	testscript := os.Getenv("O2O_TEST_SCRIPT")
	if testscript == "" {
		return nil, fmt.Errorf("no O2O test script given, set O2O_TEST_SCRIPT")
	}
	runnumber := os.Getenv("RUNNUMBER")
	if runnumber == "" {
		return nil, fmt.Errorf("no run number given, set RUNNUMBER")
	}
	tsckey := os.Getenv("TSC_KEY")
	if tsckey == "" {
		return nil, fmt.Errorf("no tsc key given, set TSC_KEY")
	}
	rskey := os.Getenv("RS_KEY")
	if rskey == "" {
		return nil, fmt.Errorf("no rs key given, set RS_KEY")
	}

	return &O2OTestArgs{
		TestScript:  testscript,
		RunNumber:   runnumber,
		TSCKey:      tsckey,
		RSKey:       rskey,
	}, nil

}

// O2OTest executes o2o test to prepDB
func O2OTest(client *ssh.Client, job *O2OTestArgs) error {
	session, err := client.NewSession()
	if err != nil {
		return err
	}
	defer session.Close()
	var stdoutBuf bytes.Buffer
	session.Stdout = &stdoutBuf
	var stderrBuf bytes.Buffer
	session.Stderr = &stdoutBuf

    c := fmt.Sprintf("source /data/O2O/L1T/%s %s %s %s", job.TestScript, job.RunNumber, job.TSCKey, job.RSKey)
	log.Printf("Running %s\n", c)
	err = session.Run(c)
	DumpStd(&stdoutBuf, &stderrBuf)

	return err
}
