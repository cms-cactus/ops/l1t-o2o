package internal

import (
	"fmt"
	"log"
	"os"
	"golang.org/x/crypto/ssh"
)

// GotoCMSConddb connects via SSH to the conddb host in the CMS network
func GotoCMSConddb(viaLXPlus bool) (*ssh.Client, error) {
	var cmsUsrClient *ssh.Client
	var err error
	if viaLXPlus {
		lxplusconf, _, _ := GetSSHConfig("lxplus.cern.ch")
		lxplus, err := ssh.Dial("tcp", "lxplus.cern.ch:22", lxplusconf)
		if err != nil {
			return nil, fmt.Errorf("could not dial lxplus: %s", err)
		}
		log.Println("connected to lxplus")
		cmsUsrClient, _, _, err = dialThrough(lxplus, "cmsusr")
	} else {
		cmsUsrConf, _, _ := GetSSHConfig("cmsusr")
		cmsUsrClient, err = ssh.Dial("tcp", "cmsusr:22", cmsUsrConf)
	}
	if err != nil {
		return nil, fmt.Errorf("could not open SSH client on cmsusr: %s", err)
	}
	log.Println("connected to cmsusr")

	conddbhost := os.Getenv("CONDDB_HOST")
	if conddbhost == "" {
		conddbhost = "cms-conddb-2"
	}
	cmsConddbClient, _, _, err := dialThrough(cmsUsrClient, conddbhost)
	if err != nil {
		return nil, fmt.Errorf("could not open SSH client on %s: %s", conddbhost, err)
	}
	log.Println("connected to", conddbhost)
	return cmsConddbClient, nil
}
