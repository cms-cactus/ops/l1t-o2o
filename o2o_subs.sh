#!/bin/bash

#CMSSW_13_0_0
#/data/O2O/L1T/o2oSubs2023_v1.sh $1 $2 $3

#CMSSW_13_0_1
#/data/O2O/L1T/o2oSubs2023_v2.sh $1 $2 $3

#CMSSW_14_0_1
#/data/O2O/L1T/o2oSubs2024_v1.sh $1 $2 $3

#CMSSW_14_0_9
/data/O2O/L1T/o2oSubs2024_v2.sh $1 $2 $3

o2ocode1=$?
#echo "Exit code "${o2ocode1}
exit ${o2ocode1}

