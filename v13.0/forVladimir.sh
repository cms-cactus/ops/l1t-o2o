. /opt/offline/cmsset_default.sh
cd CMSSW_10_0_0
cmsenv
cd ../
cmsRun ${CMSSW_BASE}/src/CondTools/L1TriggerExt/test/init_cfg.py useO2OTags=1 outputDBConnect=sqlite:./l1config.db
cmsRun ${CMSSW_BASE}/src/CondTools/L1TriggerExt/test/L1ConfigWriteSinglePayloadExt_cfg.py objectKey="OMTF_ALGO_EMPTY" objectType=L1TMuonOverlapParams recordName=L1TMuonOverlapParamsO2ORcd useO2OTags=1 outputDBConnect=sqlite:l1config.db
env TNS_ADMIN=/opt/offline/slc6_amd64_gcc493/cms/oracle-env/29/etc/ cmsRun ${CMSSW_RELEASE_BASE}/src/CondTools/L1TriggerExt/test/L1ConfigWriteSinglePayloadExt_cfg.py objectKey="7" objectType=L1TMuonEndCapForest recordName=L1TMuonEndCapForestO2ORcd useO2OTags=1 protoDBAuth=/data/O2O/L1T/ outputDBConnect=sqlite:l1config.db 

