#!/bin/bash

[ -e o2o_menu.sh ] && [ -e o2o_subs.sh ] && {
    export PATH=$PWD/build:$PATH
} \
    || echo "cd into the root dir of the l1t-o2o repo"
