#!/bin/bash

{ [[ $1 =~ \-*h ]] || [[ $1 =~ \-*help ]] || [ -z "$1" ]; } && {
    echo "Usage $(basename $0) 2021_v6 slc7_amd64_gcc900 CMSSW_12_0_1"
    echo; exit 0
}

version=$1
arch=$2
release=$3

#source scripts, no need to change unless other than CMSSW release was updated
menu_src=build/o2oMenu2022_template.sh
subs_src=build/o2oSubs2022_template.sh

#extract ARCH, RELEASE
old_arch=`cat ${menu_src} | sed -n "s/^.*ARCH=\(\S*\).*$/\1/p"`
old_release=`cat ${menu_src} | sed -n "s/^.*RELEASE=\(\S*\).*$/\1/p"`
ispatch=`cat ${menu_src} | sed -n "s/^.*\/cms\/\(\S*\)\/${RELEASE}.*$/\1/p"`

#--generate production scripts--#
cp $menu_src o2oMenu${version}.sh 
cp $subs_src o2oSubs${version}.sh 
sed -i "s/${old_arch}/${arch}/" o2oMenu${version}.sh o2oSubs${version}.sh
sed -i "s/${old_release}/${release}/" o2oMenu${version}.sh o2oSubs${version}.sh
if [[ ${release} == *"patch"* ]]; then
  sed -i "s/${ispatch}/cmssw-patch/" o2oMenu${version}.sh o2oSubs${version}.sh
else sed -i "s/${ispatch}/cmssw/" o2oMenu${version}.sh o2oSubs${version}.sh
fi
Cy='\033[1;36m'
NC='\033[0m'
echo created o2oMenu${version}.sh, o2oSubs${version}.sh
echo -e ${Cy}diff: ${menu_src} vs o2oMenu${version}.sh${NC}
colordiff ${menu_src} o2oMenu${version}.sh
echo -e ${Cy}diff: ${subs_src} vs o2oSubs${version}.sh${NC}
colordiff ${subs_src} o2oSubs${version}.sh

#--generate test scripts--#
cp o2oMenu${version}.sh o2oMenu${version}_test.sh
cp o2oSubs${version}.sh o2oSubs${version}_test.sh 
## remove all '-x' from the cloned file
sed -i -r "s/(runL1\-O2O\-iov\.sh.*)\-x/\1/" o2oMenu${version}_test.sh o2oSubs${version}_test.sh
## change --db pro to --db dev
sed -i "s/\-\-db pro/--db dev/" o2oMenu${version}_test.sh o2oSubs${version}_test.sh
## remove the exit code
sed -i "s/exit.*o2ocode.*//" o2oMenu${version}_test.sh o2oSubs${version}_test.sh
echo created o2oMenu${version}_test.sh, o2oSubs${version}_test.sh
echo -e ${Cy}diff: o2oMenu${version}.sh vs o2oMenu${version}_test.sh${NC}
colordiff o2oMenu${version}.sh o2oMenu${version}_test.sh
echo -e ${Cy}diff: o2oSubs${version}.sh vs o2oSubs${version}_test.sh${NC}
colordiff o2oSubs${version}.sh o2oSubs${version}_test.sh

#--modify master scripts--#
#sed -i "s/^\/data/#\/data/" o2o_menu.sh o2o_subs.sh
sed -i "/^o2ocode1/i #${release}\n#\/data/O2O/L1T/o2oMenu${version}.sh \$1 \$2 \$3\n" o2o_menu.sh
sed -i "/^o2ocode1/i #${release}\n#\/data/O2O/L1T/o2oSubs${version}.sh \$1 \$2 \$3\n" o2o_subs.sh
echo modified o2o_menu.sh, o2o_subs.sh
echo -e ${Cy}Changes in o2o_menu.sh${NC}
git --no-pager diff --color o2o_menu.sh 
echo -e ${Cy}Changes in o2o_subs.sh${NC}
git --no-pager diff --color o2o_subs.sh

#--modify yml file--#
yml_arch=`cat .gitlab-ci.yml | sed -n "s/^.*ARCH: \(\S*\).*$/\1/p"`
yml_release=`cat .gitlab-ci.yml | sed -n "s/^.*RELEASE: \(\S*\).*$/\1/p"`
yml_ispatch=`cat .gitlab-ci.yml | sed -n "s/^.*\/cms\/\(\S*\)\/$.*$/\1/p"`
test_version=`cat .gitlab-ci.yml | sed -n "s/^.*o2oSubs\(\S*\)_test.sh.*$/\1/p"`
## modify local test variables
sed -i "s/${yml_arch}/${arch}/" .gitlab-ci.yml
sed -i "s/${yml_release}/${release}/" .gitlab-ci.yml
if [[ ${release} == *"patch"* ]]; then
  sed -i "s/${yml_ispatch}/cmssw-patch/" .gitlab-ci.yml
else sed -i "s/${yml_ispatch}/cmssw/" .gitlab-ci.yml
fi
## redirect to new test scripts
sed -i "s/${test_version}/${version}/" .gitlab-ci.yml
echo modified .gitlab-ci.yml
echo -e ${Cy}Changes in .gitlab-ci.yml${NC}
git --no-pager diff --color .gitlab-ci.yml

#--modify spec file--#`
rpmrel=$(cat build/o2ol1t-scripts.spec | grep ^Version | sed "s/Version: //; s/.[0-9]*$//")
cmsswrel=$(echo $release | sed "s/CMSSW//; s/patch.*//; s/_//g")
fullrel=$rpmrel.$cmsswrel
sed -i "s/^Ver.*/Version: $fullrel/" build/o2ol1t-scripts.spec
sed -i "/^%changelog/{n;N;d}" build/o2ol1t-scripts.spec
packing_date=`date '+%a %b %m %Y'`
if [[ $USER == "hkwon" ]]; then
  packager="Hyejin Kwon, hj.kwon@cern.ch"
elif [[ $USER == "kpanos" ]]; then
  packager="Panos Katsoulis, panos.katsoulis@cern.ch"
elif [[ $USER == "tophan" ]]; then
  packager="Toan Phan, toan.phan@cern.ch"
fi
sed -i "/%changelog/a * $packing_date $packager\n- $fullrel Upgraded to $release" build/o2ol1t-scripts.spec
echo modified build/o2ol1t-scripts.spec
echo -e ${Cy}Changes in build/o2ol1t-scripts.spec${NC}
git --no-pager diff --color build/o2ol1t-scripts.spec

