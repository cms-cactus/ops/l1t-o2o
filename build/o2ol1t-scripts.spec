###############################################################################
# Spec file for L1T-O2O pakage (kpanos)
################################################################################
# Configured to be built by a non-root user
################################################################################
#
Summary: L1Trigger O2O scripts
Name: l1to2o-scripts
Version: 1.2.1409
Release: 1
License: none
URL: https://gitlab.cern.ch/cms-cactus/ops/l1t-o2o
Group: l1t
Packager: Panos Katsoulis
Requires: bash

# Build with the following syntax:
# LOCAL_DIR=<dir> rpmbuild --target noarch -bb <spec-file>

%description
A collection of utility scripts that are called by the function manager for updating the L1 tags in the CondDB.

%prep
## Create the build tree and copy the files from the development directories
## into the build tree.
echo $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/data/O2O/L1T/
mkdir -p $RPM_BUILD_ROOT/data/O2O/L1T/share

[ -z "$CI_PROJECT_DIR" ] && PROJ_DIR=$LOCAL_DIR || PROJ_DIR=$CI_PROJECT_DIR
cd $PROJ_DIR
cp *sh $RPM_BUILD_ROOT/data/O2O/L1T/
cp -r v2[0-9].[0-9] $RPM_BUILD_ROOT/data/O2O/L1T/
cp build/o2ol1t-scripts.spec $RPM_BUILD_ROOT/data/O2O/L1T/share

exit

%files
%attr(0755, l1emulator, l1emulator) /data/O2O/L1T/*sh
%attr(0775, l1emulator, l1emulator) /data/O2O/L1T/v2[0-9].[0-9]
%attr(0644, l1emulator, l1emulator) /data/O2O/L1T/share

%pre

%post

%postun

%clean
rm $RPM_BUILD_ROOT/data/O2O/L1T/*sh
rm -r $RPM_BUILD_ROOT/data/O2O/L1T/v*.[0-9]
rm -r $RPM_BUILD_ROOT/data/O2O/L1T/share

%changelog
* Wed Jun 06 2024 Toan Phan, toan.phan@cern.ch
- 1.2.1409 Upgraded to CMSSW_14_0_9
