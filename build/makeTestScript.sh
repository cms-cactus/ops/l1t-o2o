#!/bin/bash
set -e
[[ $1 =~ \-*debug ]] && { set -x; shift; }

[[ $1 =~ \-*help ]] && {
    printf "Usage: $(basename $0) <file>
This command will make a testing copy of the given script and appent in the end the suffix '*_test.sh'.
The changes will be:
1) remove any rederence to the 'runL1-O2O-iov.sh' file with the arg '-x'
2) change --db pro to --db dev
3) remove the return code of the input file

"
    exit 0
}

## parse the file and make a copy adding '_test.sh'
TO_MODIFY=$1
TARGET=$(echo $TO_MODIFY | sed s/\.sh/_test.sh/)
cat $TO_MODIFY > $TARGET

## remove all '-x' from the cloned file
sed -i -r "s/(runL1\-O2O\-iov\.sh.*)\-x/\1/" $TARGET
## change --db pro to --db dev
sed -i "s/\-\-db pro/--db dev/" $TARGET
## remove the exit code
sed -i "s/exit.*o2ocode.*//" $TARGET

exit 0
